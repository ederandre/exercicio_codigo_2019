<?php 
declare(strict_types=1);
namespace  Tests\Domain\Creditor;

use App\Domain\Creditor;
use PHPUnit\Framework\TestCase;

final class creditorTest extends TestCase
{
    public function creditorProvider()
    {
        return [
            ['Creditor One', 'Street One, City ONe, at 12', '1.300,05', '11/19','file_one.pdf'],
            ['Creditor two', 'Street two, City Two, at 22', '2.400,06', '12/19','file_two.pdf'],
            ['Creditor three', 'Street three, City three, at 32', '3.500,07', '10/19','file_three.pdf'],
        ];
    }

    /**
     * @dataProvider creditorProvider
     * @param $name
     * @param $address
     * @param $value
     * @param $expiry
     * @param $pathAttachment
     */
    public function testGetters($name, $address, $value, $expiry,$pathAttachment):void
    {
        $cr = new Creditor($name, $address, $value, $expiry,$pathAttachment);

        $this->assertEquals($name, $cr->getName());
        $this->assertEquals($address, $cr->getAddress());
        $this->assertEquals($value, $cr->getValue());
        $this->assertEquals($expiry, $cr->getExpiry());
    }

    /**
     * @dataProvider creditorProvider
     * @param $name
     * @param $address
     * @param $value
     * @param $expiry
     * @param $pathAttachment
     */
    public function testJsonSerialize($name, $address, $value, $expiry,$pathAttachment):void
    {
        $cr = new Creditor($name, $address, $value, $expiry,$pathAttachment);

        $expectedPayload = json_encode([
            'name' => $name,
            'address' => $address,
            'value' => $value,
            'expiry' => $expiry,
            'pathAttachment' => $pathAttachment,
        ]);

        $this->assertEquals($expectedPayload, json_encode($cr->jsonSerialize()));
    }
}