<?php
declare(strict_types=1);
namespace  Tests\Application\Utils\Parser;

use App\Application\Utils\Parser;
use PHPUnit\Framework\TestCase;

final class parserTest extends TestCase
{
    public function textProvider()
    {
        $text='
		    Bom dia,
		    <div class="moz-forward-container">
		      <div class="moz-forward-container">
		        <p>Segue meus dados de contato e informações para pagamento</p>
		        <p>Nome: Eder André Soares</p>
		        <p>Endereço: Rua Professor Freitas Cabral, 370</p>
		        <p>Valor: R$10.500,00</p>
		        <p>Vencimento:11/19</p>
		        <p></p>
		        </div>
		      </div>
		    </div>';
		return [
			  	[$text,'Nome:', 'Eder André Soares'],
			  	[$text,'Endereço:','Rua Professor Freitas Cabral, 370'],
			  	[$text,'Valor:','10.500,00'],
			  	[$text,'Vencimento:','11/19'],
		  		];
    }
	
    /**
     * @dataProvider textProvider
     * @param $text
     * @param $searchString
     * @param $expected
     */
    public function testGetParameters($text,$searchString,$expected):void
    {
        $pr = new Parser();


        $this->assertEquals($expected, $pr->getParameterFromMessageHtml($searchString, $text));
    }

}
