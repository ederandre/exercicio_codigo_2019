<?php 
declare(strict_types=1);
namespace  Tests\Core\Processor;

use App\Application\Core\ProcessMessageHtml;
use App\Domain\Creditor;
use PHPUnit\Framework\TestCase;

final class processorTest extends TestCase
{
    public function mailProvider()
    {
    	$mail1=['body'=>'
		    <div class="moz-forward-container">
		      <div class="moz-forward-container">
		        <p>Segue meus dados de contato e informações para pagamento</p>
		        <p>Nome: Eder André Soares</p>
		        <p>Endereço: Rua Professor Freitas Cabral, 370</p>
		        <p>Valor: R$10.500,00</p>
		        <p>Vencimento:11/19</p>
		        <p></p>
		        </div>
		      </div>
		    </div>',
		    'attachment'=>'file.jpg'];
		    $mail2=['body'=>'
		    <div class="moz-forward-container">
		      <div class="moz-forward-container">
		        <p>Segue meus dados de contato e informações para pagamento</p>
		        <p>Nome: Eder Andr Soares</p>
		        <p>Endereço: Rua Professor Freitas Cabral</p>
		        <p>Valor: R$100,00</p>
		        <p>Vencimento:10/19</p>
		        <p></p>
		        </div>
		      </div>
		    </div>',
		    'attachment'=>'file.pdf'];
		    $cred1=new Creditor('Eder André Soares'
		    					,'Rua Professor Freitas Cabral, 370'
		    					,'10.500,00'
		    					,'11/19'
		    					,'file.jpg');
		    $cred2=new Creditor('Eder Andr Soares'
		    					,'Rua Professor Freitas Cabral'
		    					,'100,00'
		    					,'10/19'
		    					,'file.pdf');

        return [
            [$mail1,$cred1],
            [$mail2,$cred2],
        ];
    }

    /**
     * @dataProvider mailProvider
     * @param $mail
     * @param $creditor
      */
    public function testProcessor($mail,$creditor):void
    {
        $pr = new ProcessMessageHtml();

        $this->assertEquals($creditor, $pr->processOne($mail));
 
    }


}
