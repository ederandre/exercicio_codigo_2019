<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Application\Mail\Imap as MailList;
use App\Application\Log\Logger as Log;
use App\Application\Core\ProcessMessageHtml as Process;
use App\Application\Core\Post as Api;
use App\Settings\Local\Mail as ConfMail;
use App\Settings\Local\Api as ConfAPI;
require '../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors',1);

$app = new \Slim\App();
$app->get('/', function (Request $request, Response $response) {
	$cf=new ConfMail;
	$params['host_imap_string'] = $cf->configs['host_imap_string']; 
	$params['mail_user'] = $cf->configs['mail_user'];
	$params['password'] = $cf->configs['password'];
	$queue=new MailList;
	$queue->authenticate($params);
	
	$process=new Process;
	$finalList=$process->processList($queue->getList());

	$confApi=new ConfAPI;
	$api=new Api;
	$api->setApiUri($confApi->configs['apiURI']);
	$api->setApiKey($confApi->configs['apiKey']);
	$sended=count($finalList );
	foreach($finalList as $fl) {
		$make_call =$api->submitToApi($fl->jsonSerialize());
		$response_post = json_decode($make_call, true); 
		/**
		** É requisito tratar a resposta da postagem?
		**  TO DO
		**
		**/
		$lg=new Log;
		$lg->logger(json_encode($fl->jsonSerialize()),'logs');

	}

	$queue->logout();

    $response->withStatus(200)->write("Foram enviados para a api ".$sended." movimentos de credores");
    return $response;
});
$app->group('/lastlog', function () {
   
   $this->map(['GET'], '', function (Request $request, Response $response) {
   		$lg=new Log;
        return $response->withJson(['logs' => $lg->getLastLog('logs/') ]);
    });
});

$app->run();