# Exercicio_Codigo_2019

## Exercicio de Código enviado por Robisson Oliveira

Vamos utilizar PHP 7 com o protocolo IMAP, curl e composer. 
o email e senha de email disponiblizados [neste código](https://bitbucket.org/ederandre/exercicio_codigo_2019/src/master/src/Settings/Local/Mail.php) são utilizados por mim pra fins de desnvolvimento, e esta caixa de emails já possui algumas mensagens no formato do template sugerido no exercício.

Também vamos usar o framework [slim](http://www.slimframework.com/), apenas pra acelerar a etapa de tratamento das requests e responses dos endpoints, poderia ser qualquer framework, inclusive um da casa.
 Instalação dos pacotes necessários no ubuntu:

```
apt-get install curl php-curl php-imap composer

```

## Instalando a aplicação

Depois de clonar este repositório, para instalar as dependências do projeto e ajustar as permissôes e propriedades, basta navegar até a pasta clonada e executar:

```

sudo chmod -R 755 [nome-da-pasta]
cd [nome-da-pasta]
sudo chmod -R 777 public/attachments/ public/logs/
composer install
sudo chown -R www-data:www-data [nome-da-pasta]

```
## Rodando a aplicação

O ponto de acesso publico será o diretório `public/` .
Caso a api `http://localhost/api` não seja válida, aparecerá um erro informando `Connection Failure- API disabled`, os dados de conexão da api são configuráveis no em src/Setting/Local/Api.php 

Para rodar a aplicação em desenvolvimento na porta 8000, estando na pasta da aplicação siga os comandos abaixo:

```
composer start

```
## Executando os endpoints

Abra um navegador e acesse `http://localhost:8000`, ou se remoto `http://ip_do_servidor:8000`.

O endpoint `/` - `http://ip_do_servidor:8000`- executa a rotina de leitura da caixa de email e post para a API.

O endpoint `/lastlog`- `http://ip_do_servidor:8000/lastlog` exibe o  ultimo estado do arquivo de log do processamento dos credores.

## Executando os Testes
Rode este comando para executar os testes unitarios:

```
composer test

```
