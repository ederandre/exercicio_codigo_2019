<?php
namespace App\Application\Utils;

class Parser
{
	
	public function getParameterFromMessageHtml($stringSearched, $fullText)
	{
		$result='Não encontrado';
		$lines=explode('<p>',$fullText);
		foreach($lines as $ln) {
			$string=$ln;
			if(strpos($string,$stringSearched)!==false) {
				$result=str_replace(array($stringSearched,'R$'),'',$string);
				$result=trim(strip_tags($result));
				break;

				}

		}
		
		return $result;
	}
}
