<?php 
namespace App\Application\Mail;

class Imap{

	private $inbox;
	private $searchCriteria='ALL';
	/**
	** Substituir 'ALL' por 'UNSEEN' para listar apenas as não lidas
	**/
	private $attachDirectory='attachments/';

	public function authenticate($param=array(
										'host_imap_string'=>'',
										'mail_user'=>'',
										'password'=>''
										)
	)
	{
		$this->inbox = imap_open($param['host_imap_string']
								, $param['mail_user']
								, $param['password'])
								or die("can't connect: " . imap_last_error());

	}
	
	public function logout()
	{
		imap_close($this->inbox);
	}

	public function getList()
	{ 
		$resultSet=imap_search($this->inbox,$this->searchCriteria);
		$result=array();
		foreach($resultSet as $m) {
			$body = imap_fetchbody($this->inbox, $m,1.2);
			$structure = imap_fetchstructure($this->inbox, $m);
			$attachments = array();
			if(isset($structure->parts) && count($structure->parts)) {
					for($i = 0; $i < count($structure->parts); $i++) {
						$attachments[$i] = array(
							'is_attachment' => false,
							'filename' => '',
							'name' => '',
							'attachment' => ''
						);
		 
						if($structure->parts[$i]->ifdparameters) {
							foreach($structure->parts[$i]->dparameters as $object) {
								if(strtolower($object->attribute) == 'filename') {
									$attachments[$i]['is_attachment'] = true;
									$attachments[$i]['filename'] = $object->value;
								}
							}
						}
		 
						if($structure->parts[$i]->ifparameters) {
							foreach($structure->parts[$i]->parameters as $object) {
								if(strtolower($object->attribute) == 'name') {
									$attachments[$i]['is_attachment'] = true;
									$attachments[$i]['name'] = $object->value;
								}
							}
						}
		 
						if($attachments[$i]['is_attachment']) {
							
							if($structure->parts[$i]->encoding == 3) {
								/* 4 = QUOTED-PRINTABLE encoding */
								$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
							} elseif($structure->parts[$i]->encoding == 4) {
								/* 3 = BASE64 encoding */
								$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
							}
						}
					}
				}
				foreach($attachments as $attachment) {
					if($attachment['is_attachment'] == 1) {
						$filename = $attachment['name'];
						
						if(empty($filename)) {
							$filename = $attachment['filename'];
						}
						
						if(empty($filename)) {
							$filename = time() . ".dat";
						}
		 
						$uid_message=imap_uid($this->inbox, $m);
						$attachPath=$this->attachDirectory.$uid_message . "-" . $filename;
						$fp = fopen($attachPath, "w+");
						fwrite($fp, $attachment['attachment']);
						fclose($fp);
					}
		 
				}
				$result[$uid_message]=array('body'=>$body,'attachment'=>$attachPath);


		}
		return $result;
	}
	public function setSearchCriteria($searchCriteria)
	{ 
		$this->searchCriteria=$searchCriteria;
	}
	public function setAttachDirectory($attachDirectory)
	{ 
		$this->attachDirectory=$attachDirectory;
	}
}
