<?php 
namespace App\Application\Core;

class Post
{
	private $apiUriString='http://localhost:8000';
	private $apiKey='';
	private $apiContentType='application/json';
	
	public function submitToApi($data)
	{
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$url = sprintf("%s?%s", $this->apiUriString, http_build_query($data));
		}
				
	   curl_setopt($curl, CURLOPT_URL, $url);
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		  'APIKEY: '.$this->apiUriString,
		  'Content-Type: '.$this->apiContentType,
	   ));
	   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure- API disabled");}
	   curl_close($curl);
	   return $result;
	}

	public function setApiUri($uri)
	{
		$this->apiUriString=$uri;
	}
	
	public function setApiKey($key)
	{
		$this->apiKey=$key;
	}
	public function setApiContentType($apiContentType)
	{
		$this->apiContentType=$apiContentType;
	}
}
