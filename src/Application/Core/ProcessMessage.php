<?php 
namespace App\Application\Core;

interface ProcessMessage
{	
	 public function processList($listMsg);
	 public function processOne($msg);
}
