<?php 
namespace App\Application\Core;

use App\Application\Utils\Parser as Parse;
use App\Domain\Creditor as Cr;

class ProcessMessageHtml implements ProcessMessage
{
	
	public function processList($listMsg)
	{
		$result=array();
		foreach($listMsg as $uid=>$msg) {
			$result[$uid]=$this->processOne($msg);
		}
		return $result;
	}
	public function processOne($msg=array('body'=>'','attachment'=>''))
	{
		$pars=new Parse;
			$creditor=new Cr(
				$pars->getParameterFromMessageHtml("Nome:", $msg['body']),
				$pars->getParameterFromMessageHtml("Endereço:", $msg['body']),
				$pars->getParameterFromMessageHtml("Valor:", $msg['body']),
				substr($pars->getParameterFromMessageHtml("Vencimento:", $msg['body']),0,5),
				$msg['attachment']
			);
			
			return $creditor;
	}
}
