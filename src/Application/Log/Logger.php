<?php 
namespace App\Application\Log;

class Logger
{
	
	public function Logger($msg,$path='')
	{		
 		$date=date("d-m-y");
		$datetime = date("d-m-y_H_i_s");
		$file = "Logger_$date.txt";
		$line = "[$datetime]: $msg \n";
		$handler = fopen("$path".DIRECTORY_SEPARATOR."$file", "a+");
		fwrite($handler, $line);
		fclose($handler);
 
	}

	public function getLastLog($path='')
	{		
 		$result=array();
		if(is_dir($path)) {
			$dir=dir($path);
			while ($file=$dir->read()) {
			  if($file!='..' && $file!='.') {
			  	$result[date('YmdHis',filemtime($path.$file))]=$path.$file;
			  }
			}
			$dir->close();
		}
		krsort($result,SORT_STRING);
		$current_result=current($result);
		return file($current_result, FILE_IGNORE_NEW_LINES);
	}
	
}
