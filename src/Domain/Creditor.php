<?php 
namespace App\Domain;

class Creditor
{
	
	private $name;
	private $address;
	private $value;
	private $expiry;
	private $pathAttachment;

	public function __construct( $name,  $address,  $value, $expiry, $pathAttachment)
    {
        $this->name = $name;
        $this->address = $address;
        $this->value = $value;
        $this->expiry = $expiry;
        $this->pathAttachment = $pathAttachment;
    }
	
	public function jsonSerialize()
	{
		return array(
			'name'=> $this->name,
			'address'=> $this->address,
			'value'=> $this->value,
			'expiry'=> $this->expiry,
			'pathAttachment'=> $this->pathAttachment
		);
	}
	
	public function getName()
	{
		return $this->name;
	}
	public function setName($name)
	{
		$this->name=$name;
	}
	public function getAddress()
	{
		return $this->address;
	}
	public function setAddress($address)
	{
		$this->address=$address;
	}
	public function getValue()
	{
		return $this->value;
	}
	public function setValue($value)
	{
		$this->value=$value;
	}
	public function getExpiry()
	{
		return $this->expiry;
	}
	public function setExpiry($expiry)
	{
		$this->expiry=$expiry;
	}
	public function getPathAttachment()
	{
		return $this->pathAttachment;
	}
	public function setPathAttachment($pathAttachment)
	{
		$this->pathAttachment=$pathAttachment;
	}
}
